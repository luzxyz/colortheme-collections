# Color theme collections for terminals

`2-clause 'simplified' BSD Licensed, aka just write the copyright and i won't travel to your home to play aggresive tic tac toe.`

Consult linceses [@opensource.org](https://opensource.org/licenses/BSD-2-Clause). If you find your theme configured here somehow, please let me know in the issues page!

On the Alacritty folder, there's a mirror from another repository of alacritty themes [upstream](https://github.com/eendroroy/alacritty-theme)

**How to**: Each folder have a configuration file or file with its custom color scheme, you can either copy and paste it where it belongs or create symlinks or anyway you find out more comfortable.

> If you plan to clone this repository without images, go to the branch **woutimgs**

**READ!**
  * This is not a guide on how to use/configure terminals!
  * Mind that the code mantained is not guaranteed to be headache-free. 
  * Some of the configuration files may have content that may cause trouble.
  * It is advisable to create symlinks to link each configuration if you change often.
  * Go to each terminal's docs to find out how they work.

## Easy go-to

At this date (JAN 2023) I only use Alacritty, starting to experiment with wezterm. Most of the themes are up to date with Alacritty.

Name | Theme | Forked? | Tags
--- | --- | --- | ---
Palemoon | dark | yes | common colorscheme
SEL | dark | no | curated high contrast 
Schemes & Leisure | dark | no | nature, autumn
icewire | dark | no | monochrome(blue)
base16 firefly | dark | no | common colorscheme, base16
base16 firefly Light | light | no | common colorscheme, base16

### Palemoon
Palenight fork from [this guy](https://github.com/drewtempelmeyer/palenight.vim)

Well tought color saturation and contrast choicing.

view here [palemoon]

### SEL
Super Edge Lord

202301 Remade: High contrast colors in mind at the same time thinking about user's eyes.

view here [sel]

### Schemes & Leisure

Inspired from a sun-set in the middle of an autumn lakeside.
202301 Update: corrected color balance.

view here [sal]

### Icewire

A subtle opaque monochrome theme, tainted by an arctic machine.

view here [icewire]

### Base16 firefly 

Common color schemes done for nocturnal people with hot takes on fireflies. Take care of your eyes!

view here [b16firefly]

### Base16 firefly (Light)

Common color schemes done for diurnal people with cold takes on fireflies. Take care of your eyes!

view here [b16fireflyL]

### Mirrors

There is a mirror on the Alacritty folder with other people's themes. They're not well mantained by myself as I don't keep track on them often.

[sel]: https://codeberg.org/luzxyz/colortheme-collections/raw/branch/master/picdemos/SEL202301.png
[palemoon]: https://codeberg.org/luzxyz/colortheme-collections/raw/branch/master/picdemos/palemoon202301.png
[icewire]: https://codeberg.org/luzxyz/colortheme-collections/raw/branch/master/picdemos/icewire202301.png
[sal]: https://codeberg.org/luzxyz/colortheme-collections/raw/branch/master/picdemos/SaL202301.png
[b16firefly]: https://codeberg.org/luzxyz/colortheme-collections/raw/branch/master/picdemos/firefly.png
[b16fireflyL]: https://codeberg.org/luzxyz/colortheme-collections/raw/branch/master/picdemos/L_firefly.png
